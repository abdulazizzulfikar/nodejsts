import mongoose from 'mongoose';

async function connect(){
    try {
        mongoose.connect('mongodb://localhost:27017/nodets',{
            useNewUrlParser : true,
            useUnifiedTopology: true
        });
        console.log(">>> DB CONNECTED");
    } catch (error) {
        console.log(error);
    }
}

export default connect;